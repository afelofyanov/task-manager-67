package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ServerAboutRequest extends AbstractRequest {
}
