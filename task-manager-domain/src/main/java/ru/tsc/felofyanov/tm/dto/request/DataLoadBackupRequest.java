package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataLoadBackupRequest extends AbstractUserRequest {

    public DataLoadBackupRequest(@Nullable String token) {
        super(token);
    }
}
