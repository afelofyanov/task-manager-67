package ru.tsc.felofyanov.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! TaskId is empty...");
    }
}
