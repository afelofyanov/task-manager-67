package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataLoadJsonFasterXmlResponse extends AbstractResponse {
}
