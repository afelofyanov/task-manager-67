package ru.tsc.felofyanov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.service.ISenderService;

import javax.jms.*;

@Getter
public final class SenderService implements ISenderService {

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);

    @NotNull
    private final BrokerService broker = new BrokerService();

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final Queue destination;

    @NotNull
    private final MessageProducer producer;

    @SneakyThrows
    public SenderService() {
        broker.addConnector("tcp://localhost:61616");
        broker.start();
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue("TM_LOG");
        producer = session.createProducer(destination);
    }

    @Override
    @SneakyThrows
    public void send(@NotNull final String message) {
        producer.send(session.createTextMessage(message));
    }
}
