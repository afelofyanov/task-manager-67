package ru.tsc.felofyanov.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;

@NoRepositoryBean
public interface IDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}