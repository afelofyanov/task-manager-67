package ru.tsc.felofyanov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.listener.LoggerListener;
import ru.tsc.felofyanov.tm.service.ReceiverService;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    @SneakyThrows
    public void run(@Nullable final String[] args) {
        receiverService.receive(new LoggerListener());
    }
}
