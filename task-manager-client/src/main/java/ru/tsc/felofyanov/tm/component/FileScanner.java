package ru.tsc.felofyanov.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.listener.AbstractListener;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private final File folder = new File("./tmp");

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private void init() {
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            for (@NotNull final AbstractListener listener : listeners)
                if (fileName.equals(listener.getName())) {
                    try {
                        file.delete();
                        publisher.publishEvent(new ConsoleEvent(fileName));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    public void start() {
        init();
    }
}
