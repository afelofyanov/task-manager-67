package ru.tsc.felofyanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.dto.request.TaskGetByIndexRequest;
import ru.tsc.felofyanov.tm.dto.response.TaskGetByIndexResponse;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIndexListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by index.";
    }

    @Override
    @EventListener(condition = "@taskShowByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken(), index);
        @NotNull final TaskGetByIndexResponse response = getTaskEndpoint().getTaskByIndex(request);
        @Nullable final TaskDTO task = response.getTask();
        showTask(task);
    }
}
