package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.IProjectDTORepository;
import ru.tsc.felofyanov.tm.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectDTOService {

    @NotNull
    @Autowired
    private IProjectDTORepository repository;

    public void create() {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(
                "New project: " + System.currentTimeMillis(),
                "Description " + System.currentTimeMillis());
        save(projectDTO);
    }

    public ProjectDTO add(@NotNull final ProjectDTO project) {
        save(project);
        return project;
    }

    public ProjectDTO save(@NotNull final ProjectDTO project) {
        return repository.save(project);
    }

    public List<ProjectDTO> save(@NotNull final List<ProjectDTO> projects) {
        for (ProjectDTO project : projects) repository.save(project);
        return projects;
    }

    public Collection<ProjectDTO> findAll() {
        return repository.findAll();
    }

    public ProjectDTO findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    public void removeById(String id) {
        repository.deleteById(id);
    }

    public void remove(@NotNull final ProjectDTO project) {
        repository.delete(project);
    }

    public void remove(@NotNull final List<ProjectDTO> projects) {
        projects.stream().forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return repository.existsById(id);
    }

    public void clear() {
        repository.deleteAll();
    }

    public long count() {
        return repository.count();
    }
}
