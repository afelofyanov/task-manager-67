package ru.tsc.felofyanov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.felofyanov.tm.model.TaskDTO;
import ru.tsc.felofyanov.tm.service.TaskDTOService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.felofyanov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @Autowired
    private TaskDTOService service;

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task")
            @RequestBody TaskDTO task
    ) {
        service.remove(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody List<TaskDTO> tasks
    ) {
        service.remove(tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        service.clear();
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        service.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        return service.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() {
        return service.findAll().stream().collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDTO findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        return service.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDTO save(
            @WebParam(name = "task")
            @RequestBody TaskDTO task
    ) {
        return service.save(task);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public List<TaskDTO> saveAll(
            @WebParam(name = "tasks")
            @RequestBody List<TaskDTO> tasks
    ) {
        return service.save(tasks);
    }
}
