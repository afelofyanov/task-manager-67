package ru.tsc.felofyanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.felofyanov.tm.api.model.IWBS;
import ru.tsc.felofyanov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractWbsDTO extends AbstractModelDTO implements IWBS {

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(nullable = false)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    @Nullable
    @Column(name = "start_dt")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @Nullable
    @Column(name = "end_dt")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public AbstractWbsDTO(@NotNull final String name) {
        this.name = name;
    }

    public AbstractWbsDTO(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public AbstractWbsDTO(@NotNull final String name, @NotNull final Status status, @NotNull final Date created) {
        this.name = name;
        this.status = status;
        this.created = created;
    }

    @Override
    public String toString() {
        return name + " (" + getId() + ") : " + getStatus().getDisplayName();
    }
}
