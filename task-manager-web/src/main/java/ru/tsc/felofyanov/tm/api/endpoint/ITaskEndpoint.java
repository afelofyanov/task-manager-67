package ru.tsc.felofyanov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task")
            @RequestBody TaskDTO task
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody List<TaskDTO> tasks
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDTO findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    TaskDTO save(
            @WebParam(name = "task")
            @RequestBody TaskDTO task
    );

    @WebMethod
    @PostMapping("/saveAll")
    List<TaskDTO> saveAll(
            @WebParam(name = "tasks")
            @RequestBody List<TaskDTO> tasks
    );
}
