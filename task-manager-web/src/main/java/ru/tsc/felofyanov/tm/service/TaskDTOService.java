package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.ITaskDTORepository;
import ru.tsc.felofyanov.tm.model.TaskDTO;

import java.util.Collection;
import java.util.List;

@Service
public class TaskDTOService {

    @NotNull
    @Autowired
    private ITaskDTORepository repository;

    public void create() {
        @NotNull final TaskDTO task = new TaskDTO(
                "New task: " + System.currentTimeMillis(),
                "Description " + System.currentTimeMillis());
        save(task);
    }

    public TaskDTO add(@NotNull final TaskDTO task) {
        save(task);
        return task;
    }

    public TaskDTO save(@NotNull final TaskDTO task) {
        return repository.save(task);
    }

    public List<TaskDTO> save(@NotNull final List<TaskDTO> tasks) {
        for (TaskDTO task : tasks) repository.save(task);
        return tasks;
    }

    public Collection<TaskDTO> findAll() {
        return repository.findAll();
    }

    public TaskDTO findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    public void removeById(String id) {
        repository.deleteById(id);
    }

    public void remove(@NotNull final TaskDTO task) {
        repository.delete(task);
    }

    public void remove(@NotNull final List<TaskDTO> tasks) {
        tasks.stream().forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return repository.existsById(id);
    }

    public void clear() {
        repository.deleteAll();
    }

    public long count() {
        return repository.count();
    }
}
